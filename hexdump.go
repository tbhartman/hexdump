package main

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintln(os.Stdout, "usage: hexdump [FILENAME|-]")
		log.Fatal("invalid arguments")
	}
	var b []byte
	var err error
	if os.Args[1] == "-" {
		b, err = ioutil.ReadAll(os.Stdin)
	} else {
		b, err = ioutil.ReadFile(os.Args[1])
	}
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(hex.Dump(b))
}
